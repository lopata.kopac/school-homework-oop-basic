﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public interface IShapes
    {
        double Contents();
        double Circuit();
    }

    class Cube : IShapes
    {
        public double A { get; set; }
        public double B { get; set; }

        public double Contents()
        {
            return A * B;
        }

        public double Circuit()
        {
            return 2 * A + 2 * B;
        }

        public Cube()
        {

        }

        public Cube(double a, double b)
        {
            A = a;
            B = b;
        }
    }

    class Triangle : IShapes
    {
        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }

        public double Contents()
        {
            double s = (A + B + C);
            return Math.Pow(s * (s - A) * (s - B) * (s - C), 2);
        }

        public double Circuit()
        {
            return A + B + C;
        }

        public Triangle()
        {

        }

        public Triangle(double a, double b, double c)
        {
            A = a;
            B = b;
            C = C;
        }
    }
}
