﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class Witdraw
    {
        public Witdraw()
        {
            System.Console.WriteLine("Witdrav Constructor");
        }

        public virtual bool CanWitdraw(int value)
        {
            System.Console.WriteLine("Witdrav Method");
            return false;
        }
    }

    public class Anccount : Witdraw
    {
        public int Value { get; set; } = 0;
        public Anccount()
        {
            System.Console.WriteLine("Anccount Constructor");
        }

        public Anccount(int value)
        {
            Value = value;
            System.Console.WriteLine("Anccount Constructor");
        }

        public override bool CanWitdraw(int value)
        {
            System.Console.WriteLine("Anccount Method");
            return (Value > value);
        }
    }
}
