﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public interface IRegisteredCostomer : ICostomer
    {
        List<Oder> History { get; set; }
    }
}
