﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class Notebook : IProduct
    {
        public string Type { get; set; } = "Notebook";
        public int Cost { get; set; } = 20;

        public string ToString()
        {
            return $"typ: {Type}, Cena: {Cost}";
        }
    }
}
