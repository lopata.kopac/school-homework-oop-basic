﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public interface IProduct : IStringOutput
    {
        string Type { get; set; }
        int Cost { get; set; }
    }
}
