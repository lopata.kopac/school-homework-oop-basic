﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class Mobile : IProduct
    {
        public string Type { get; set; } = "Notebook";
        public int Cost { get; set; } = 10;

        public string ToString()
        {
            return $"typ: {Type}, Cena: {Cost}";
        }
    }
}
