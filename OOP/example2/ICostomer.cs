﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public interface ICostomer : IStringOutput
    {
        string Name { get; set; }
        void NewOder(Oder oder);
        void NewOder(IProduct[] products);
    }
}
