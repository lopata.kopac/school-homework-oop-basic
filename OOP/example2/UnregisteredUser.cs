﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    internal class UnregisteredUser : ICostomer
    {
        public string Name { get; set; }

        public void NewOder(Oder oder)
        {
            System.Console.WriteLine($"Uyivatel {Name} ma novu obienavku");
            System.Console.WriteLine(oder.ToString());
        }

        public void NewOder(IProduct[] products)
        {
            Oder oder = new Oder();
            System.Console.WriteLine($"Uzivatel {Name} ma novu obienavku");
            System.Console.WriteLine(oder.ToString());
        }

        public string ToString()
        {
            return $"Uyivatel {Name} nema historiu";
        }
    }
}
