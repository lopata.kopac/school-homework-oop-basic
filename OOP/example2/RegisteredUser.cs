﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class RegisteredUser : IRegisteredCostomer
    {
        public string Name { get; set; }
        public List<Oder> History { get; set; } = new List<Oder>();

        public void NewOder(Oder oder)
        {
            History.Add(oder);

            System.Console.WriteLine($"Uzivatel {Name} ma novu obienavku");
            System.Console.WriteLine(oder.ToString());
        }

        public void NewOder(IProduct[] products)
        {
            Oder oder = new Oder();
            oder.AddProduts(products);

            History.Add(oder);

            System.Console.WriteLine($"Uzivatel {Name} ma novu obienavku");
            System.Console.WriteLine(oder.ToString());
        }

        public string ToString()
        {
            string result = $"Historia uzivatela {Name}\n";

            foreach (var item in History)
            {
                result += item.ToString();
                
            }

            return result;
        }
    }
}
