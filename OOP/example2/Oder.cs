﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class Oder : IStringOutput
    {
        List<IProduct> Products { get; set; } = new List<IProduct>();

        public void AddProduts(IProduct[] products)
        {
            foreach (var item in products)
                Products.Add(item);
        }

        public string ToString()
        {
            string result = "";
            foreach (var item in Products)
                result += item.ToString() + "\n";

            return result += "\n";
        }
    }
}