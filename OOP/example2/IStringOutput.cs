﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public interface IStringOutput
    {
        string ToString();
    }
}
