﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class Singleton<T> where T : class
    {
        public Singleton()
        {
            if (instance == null)
            {
                T t = this as T;
                instance = t;
            }
        }
        private static T instance;

        public static T Instance
        {
            get
            {
                return instance;
            }
        }
    }

    public class GlobalClass  : Singleton<GlobalClass>
    {
    }
}
