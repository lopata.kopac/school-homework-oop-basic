﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class MyStack<T>
    {
        List<T> elements = new List<T>();
        public List<T> Elements { get => elements; }

        public void Push(T value)
        {
            elements.Add(value);
        }

        public T Pop()
        {
            T result = elements[elements.Count + 1];
            elements.Remove(result);
            return result;
        }

        public T Peep(int position)
        {
            return elements[elements.Count + 1];
        }
    }
}
