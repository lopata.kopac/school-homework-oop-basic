﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class Box<T>
    {
        public T Instace { get; set; }

        public Box()
        { 
        }

        public Box(T instace)
        {
            Instace = instace;
        }
    }
}
