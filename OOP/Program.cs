﻿using System;

namespace OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            Example2();
        }

        private static void Example1()
        {
            Witdraw ancount = new Anccount(10);
            System.Console.WriteLine("Mozem vybrat 11$? ");
            System.Console.WriteLine(ancount.CanWitdraw(10) ? "Ano" : "Nie");
        }

        private static void Example2()
        {
            ICostomer costomer = new RegisteredUser { Name = "Oto"};

            IProduct[] produts = { new Tablet(), new Notebook()};
            costomer.NewOder(produts);

            produts = new IProduct[]{ new Mobile(), new Mobile(), new Notebook() };
            costomer.NewOder(produts);
            System.Console.WriteLine(costomer.ToString());

            ICostomer unregisteredCustomer = new UnregisteredUser { Name = "Oto" };

            produts = new IProduct[] { new Mobile(), new Mobile(), new Notebook() };
            unregisteredCustomer.NewOder(produts);

            System.Console.WriteLine(unregisteredCustomer.ToString());
        }

        private static void Example3()
        {
            int i = 5;
            Box<int> box = new Box<int>(i);

            System.Console.WriteLine($"Zisti typ instce: {box.Instace.GetType()}");

            Triangle triangle = new Triangle(10, 5, 3);
            Box<Triangle> triangleBox = new Box<Triangle>(triangle);

            System.Console.WriteLine($"Zisti typ instce: {triangleBox.Instace.GetType()}");
            System.Console.WriteLine($"Hodnota trouhonika A: {triangleBox.Instace.A}");
            System.Console.WriteLine();
        }
    }
}
