#pragma once
#include <iostream>

class Account
{
public:
	Account();
	virtual ~Account();

	virtual bool CanWitdraw(double value);
private:
	double balance = 100;
};

class CreditAccount : public Account
{
public:
	CreditAccount();
	virtual ~CreditAccount();

	virtual bool CanWitdraw(double value);
};

