#include "MyStack.h"

template<class T>
inline MyStack<T>::MyStack(int size)
{
	values = new T[size];
	capacity = size;
	top = -1;
}

template<class T>
inline MyStack<T>::~MyStack()
{
	delete[] values;
}

template<class T>
inline void MyStack<T>::Push(T value)
{
	if (top + 1 <= capacity) {
		top++;
		value[top] = value;
	}
	else
	{
		//implement capatity increase
	}
}

template<class T>
inline T MyStack<T>::Pop()
{
	if (!isEmpty())
	{
		top--;
		return values[top + 1];
	}

	return -1;
}

template<class T>
inline bool MyStack<T>::isEmpty()
{
	return (top >= 0);
}
