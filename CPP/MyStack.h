#pragma once

template <class T>
class MyStack 
{
public:
	MyStack(int size);
	~MyStack();
	void Push(T value);
	T Pop();
	bool isEmpty();
private:
	T *values;
	int capacity;
	int top;
};
