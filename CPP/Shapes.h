#pragma once
#include <math.h>  
class  Shapes
{
public:
	virtual double contents() = 0;
	virtual double circuit() = 0;
};

class Cube : public Shapes 
{
public:
	Cube(double A, double B);
	double contents();
	double circuit();

	double getA();
	double getB();
	void setA(double value);
	void setB(double value);
private:
	double a, b;
};

class Triangle : public Shapes
{
public:
	Triangle();
	Triangle(double A, double B, double C);
	double contents();
	double circuit();

	double getA();
	double getB();
	double getC();
	void setA(double value);
	void setB(double value);
	void setV(double value);
private:
	double a, b, c;
};