#include "Shapes.h"

Cube::Cube(double A, double B)
{
	a = A;
	b = B;
}

double Cube::contents()
{
	return a * b;
}

double Cube::circuit()
{
	return 2 * a + 2 * b;;
}

double Cube::getA()
{
	return a;
}

double Cube::getB()
{
	return b;
}

void Cube::setA(double value)
{
	a = value;
}

void Cube::setB(double value)
{
	b = value;
}

Triangle::Triangle()
{
}

Triangle::Triangle(double A, double B, double C)
{
	a = A;
	b = B;
	c = C;
}

double Triangle::contents()
{
	double s = (a + b + c);
	return pow(s * (s - a) * (s - b) * (s - c), 2);
	
}

double Triangle::circuit()
{
	return a + b + c;
}

double Triangle::getA()
{
	return a;
}

double Triangle::getB()
{
	return b;
}

double Triangle::getC()
{
	return c;
}

void Triangle::setA(double value)
{
	a = value;
}

void Triangle::setB(double value)
{
	b = value;
}

void Triangle::setV(double value)
{
	c = value;
}
